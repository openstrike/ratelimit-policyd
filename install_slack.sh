#!/bin/bash

# Installation script for Slackware

if [ ! -e /etc/slackware-version ];
then
	echo /etc/slackware-version not found - assuming not slackware. Aborting.
	exit 1
fi

# get current directory
DIR="$( cd "$( dirname "$0" )" && pwd )"
cd "$DIR"

# assure our logfile belongs to user postfix
touch /var/log/ratelimit-policyd.log
chown postfix:postfix /var/log/ratelimit-policyd.log

# install script
install -m 0755 -C ratelimit-policyd.pl /usr/local/bin/

# install init script
install -m 0755 -C rc.d/rc.ratelimit-policyd /etc/rc.d/

# install config
install -m 0644 -C ratelimit-policyd.cfg /usr/local/etc/

# Make PID dir
mkdir -m 0755 /var/run/ratelimit-policyd
chown postfix /var/run/ratelimit-policyd

# install logrotation configuration
install -m 0644 -C logrotate.d/ratelimit-policyd /etc/logrotate.d/
